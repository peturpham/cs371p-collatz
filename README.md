# CS371p: Object-Oriented Programming Collatz Repo

* Name: Peter Pham

* EID: pnp392

* GitLab ID: peturpham

* HackerRank ID: peturpham

* Git SHA: 30c08f67e59c0a8e24c91218f47d53d5622a1ce1

* GitLab Pipelines: https://gitlab.com/peturpham/cs371p-collatz/-/pipelines

* Estimated completion time: 5

* Actual completion time: 10

* Comments: N/A
