// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "Collatz.hpp"

using namespace std;

array<int, 1000000> cache;

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i;
    int j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ------------
// collatz_eval
// ------------

/* OPTIMIZATIONS:

*1. 3n+1/2

2. let m = (e / 2) + 1.

If b < m, then max_cycle_length(b, e) = max_cycle_length(m, e).

3. lazy cache
4. eager cache
5. meta cache


*/
tuple<int, int, int> collatz_eval (const pair<int, int>& p) {

    int i;
    int j;
    tie(i, j) = p;

    assert (i > 0 && i < 1000000);
    assert (j > 0 && j < 1000000);

    int n = i;
    int e = j;

    /* Swap upper and lower bound if they are out of order */
    if (j < i)
    {
        n = j;
        e = i;
    }

    /* Range optimization */
    if (n < e/2+1)
        n = e/2+1;

    int max = -1;
    for (; n <= e; n++)
    {
        /* Determine collatz cycle length of n */
        long k = n;
        int count = cache_lookup(n);
        if (count == 0)
        {
            count = 1;
            while (k > 1)
            {
                count++;
                if (k % 2 == 0)
                    k /= 2;
                else
                {
                    k = k + (k >> 1) + 1; // (3n+1)/2 optimization
                    count++;
                }
            }
            cache_add(n, count);
        }

        if (max < count)
            max = count;
    }

    assert (max > 0);

    return make_tuple(i, j, max);
}

// -------------
// cache_add
// -------------

void cache_add(int n, int c)
{
    assert (n > 0 && n < 1000000);
    cache[n] = c;
}

// -------------
// cache_lookup
// -------------

int cache_lookup(int n)
{
    assert (n > 0 && n < 1000000);
    return cache.at(n);
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& sout, const tuple<int, int, int>& t) {
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& sin, ostream& sout) {
    string s;
    cache.fill(0);
    while (getline(sin, s))
        collatz_print(sout, collatz_eval(collatz_read(s)));
}
